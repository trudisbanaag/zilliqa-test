'use strict';

module.exports = (app) => {  
    const token = require('./controllers/token');

    app.route('/tokens/:tokenId/supply').get(token.get_token_supply);
    app.route('/tokens/:tokenId/info').get(token.get_token_info);
    app.route('/tokens/:tokenId/users').get(token.get_token_users);
    app.route('/tokens/:tokenId/:address').get(token.get_balance_by_address);
};