'use strict';

const utils = require('./utils');
const { Zilliqa } = require('@zilliqa-js/zilliqa');
const path = process.env.ZILLIQA_HOST;
//const zilliqa = new Zilliqa('https://dev-api.zilliqa.com');
const zilliqa = new Zilliqa(path);

const availableToken = ['5938fc8af82250ad6cf1da3bb92f4aa005cb2717'];

exports.get_token_supply = async (req, res) => {
    if (!utils.isTokenAllowed(req.params.tokenId, availableToken)) {
        res.status(400).json({message:"token not found"});
    }
    try {    
        let token = "0x" + req.params.tokenId;
        let state = await zilliqa.blockchain.getSmartContractInit(token);
        if (state.result != null) {
            let total = Array.from(state.result).filter((item) => item.vname === 'total_tokens');
            res.json({total_tokens:total[0].value});
        } else {
            res.status(400).json({message:"token not found"});
        }
    } catch (err) {
        console.log(err);
    }
};

exports.get_token_info = async (req, res) => {
    if (!utils.isTokenAllowed(req.params.tokenId, availableToken)) {
        res.status(400).json({message:"token not found"});
    }
    try {
        let token = "0x" + req.params.tokenId;
        let state = await zilliqa.blockchain.getSmartContractInit(token);
        if (state.result != null) {
            let name = Array.from(state.result).filter((item) => item.vname === 'name');
            let symbol = Array.from(state.result).filter((item) => item.vname === 'symbol');

            res.json({name:name[0].value, symbol:symbol[0].value});
        } else {
            res.status(400).json({message:"token not found"});
        }
    } catch (err) {
        console.log(err);
    }  
};

exports.get_balance_by_address = async (req, res) => {
    if (!utils.isTokenAllowed(req.params.tokenId, availableToken)) {
        res.status(400).json({message:"token not found"});
    }
    try {
        let token = "0x" + req.params.tokenId;
        let address = "0x" + req.params.address;

        let state = await zilliqa.blockchain.getSmartContractSubState(
            token,
            'balances',
            [address],
        );

        if (state.result != null) {
            let addressBalance =  Object.entries(state.result.balances).map(item => item[1]);
            res.json({balance:addressBalance[0]});
        } else {
            res.json({balance:"0"});
        }
    } catch (err) {
        console.log(err);
    }
};

exports.get_token_users = async (req, res) => {
    if (!utils.isTokenAllowed(req.params.tokenId, availableToken)) {
        res.status(400).json({message:"token not found"});
    }
    try {
        let contract = zilliqa.contracts.at(req.params.tokenId);
        let contractState = await contract.getState();

        let balances = contractState.balances;
        let sortedBalances = utils.sortBalances(balances);
        res.json({users:sortedBalances});
    } catch (err) {
        console.log(err);
    }    
};