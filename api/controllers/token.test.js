const utils = require('./utils');

test('sort should sort balances', () => {
    expect(true).toBe(true);
    const input = {
        'a': '2',
        'ere': '1',
        'c': '3',
    };
    const out = utils.sortBalances(input);
    const expected = {"c":"3","a":"2","ere":"1"}
    expect(JSON.stringify(out)).toBe(JSON.stringify(expected));

    // const input2 = {
    //     'a': '2',
    //     'ere': '1',
    //     'c': '3',
    //     'ere': '4',
    // };
    // const out2 = sortBalances(input2);
    // const expected2 = {"ere":"1","a":"2","c":"3", "ere":"4"}
    // console.log('Output', JSON.stringify(out2));
    // expect(JSON.stringify(out2)).toBe(JSON.stringify(expected2));

});
