const isTokenAllowed = (tokenId, availableToken) => {
    let idx = availableToken.indexOf(tokenId);
    if (idx < 0) {
       return false;
    }
    return true;
}

const sortBalances = (jsonObj) => {
    let arr = [];
    // object to array
    for (let [key, value] of Object.entries(jsonObj)) {
        arr.push({ [key]: value });
    }

    arr.sort((a, b) => {
        let aVal = parseInt(a[Object.keys(a)[0]]);
        let bVal = parseInt(b[Object.keys(b)[0]]);
        if (aVal > bVal) {
            return -1;
        }
        if (bVal > aVal) {
            return 1;
        }
        return 0;
    });

    // array to object
    const sortedObj = {};
    for (let i = 0; i < arr.length; i++) {
        let key = Object.keys(arr[i])[0];
        sortedObj[key] = arr[i][key];
    }

    return sortedObj;
}

module.exports = {
    isTokenAllowed,
    sortBalances
}