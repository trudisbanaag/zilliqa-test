# Zilliqa Test (Server)

## Usage

### Install

    git clone https://bitbucket.org/trudisbanaag/zilliqa-test
    docker build -t zilliqa/server .

### Run

Run the image, port mappings and the APP_PORT values must be the same:

    docker run -p 3001:3001 -e ZILLIQA_HOST='https://dev-api.zilliqa.com' -e APP_PORT=3001 zilliqa/server


## API

    Documentation about the API may be found at /api-docs of the server.

## Running without Docker

Please set in your environment ZILLIQA_HOST and APP_PORT to the appropriate values. Then build the server:

```bash
npm install
```
