const express = require('express'),
      app = express(),
      routes = require('./api/routes'),
      cors = require('cors'),
      swaggerUi = require('swagger-ui-express'),
      port = process.env.APP_PORT || 3001;

app.use(cors());
// app.use((req, res, next) => {
//     if (req.headers['authorization'] != "Basic dGVzdDoxMjPCow=="){
//         res.status(401).send({ message: 'forbidden' })
//     } else {
//         next();
//     }
// }) 

routes(app);

const YAML = require('yamljs');
const swaggerDocument = YAML.load('./swagger.yaml');
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use((req, res) => {
    res.status(404).send({url: req.originalUrl + ' not found'})
});

app.listen(port);

console.log('API server started on: ' + port);
